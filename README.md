## Solar

A script for uploading Huawei Solar inverter power generation data to [pvoutput.org](https://pvoutput.org)

Note: this script was developed to run on a Linux machine. It should run on Windows and OSX, but has not been tested there. Feedback on how to
do this would be appreciated so instructions can be included here.


## Requirements
1. `solar` requires the Erlang language to be installed.
1. A copy of the `solar` script in contained in this repository. The repository can be downloaded from the download link above.
1. A PVOutput account
1. A Huawei Fusion Solar account and an API user account


### Installing Erlang

Installation instructions can be found at the Erlang [downloads](http://www.erlang.org/downloads/) page. 

### Downloading the script

1. [Download](https://gitlab.com/martinstannard/solar/blob/master/solar) the script
1. Set the script to be executable. In Linux and OSX this can be achieved with the command `chmod 755 solar`

### PVOutput settings

Solar requires the API key and System Id from your PVOutput setting page.

These needs to be set as environment variables for your operating system:

```bash
export PVOUTPUT_APIKEY=your_output_key
export PVOUTPUT_ID=your_system_id
```

### Huawei settings

#### Setting up your accounts

In order to download the daily power generation data you need an account on the Huawei Fusion Solar site. I've uploaded the [instructions](https://gitlab.com/martinstannard/solar/blob/master/docs/2018INVL001%20How%20to%20create%20an%20account%20on%20NetEco%20AU.PDF) that were sent to me by Huawei technical support, and I can't find any public location for them.

After creating your account, login and go to the Setting page. Here you will need to create a new API user account for accessing the power generation data for your inverter. This account will allow the Solar script to connect to the Huawei API and download daily data. This data is then transformed by the script and uploaded to PVOutput.

So you should now have 2 Huawei logins. The first is the account you used to setup the account, the second is the API user account you have just created. You will need to setup environment variables for the API user account:

```
export INVERTER_USERNAME=your_api_account_name
export INVERTER_PASSWORD=your_api_account_password
```

#### Obtaining your PlantID

You will now need to run the `solar` script with the `-i` parameter to retrieve the `plantId` of your system from Huawei.

```bash
➜ ./solar -i
running info
[
  %{
    "co2reduce" => "3.963 t",
    "currentPower" => "0.0 kW",
    "dayEnergy" => "45.220 kWh",
    "income" => "492.91 AUD",
    "inverterNum" => 1,
    "plantName" => "HomeSolar",
    "plantid" => 123456,    #  <<<<<<<<<<<<<< this is your plantId
    "specificEnergy" => "6.85 kWh/kWp",
    "totalEnergy" => "3.975 MWh",
    "totalRate" => "5.000 kW",
    "totalStringPower" => "6.600 kWp"
  }
]
```

Now you need to set the plantID environment variable so `solar` can retrieve data for your system:

```bash
export INVERTER_PLANTID=your_plantid
```

## Usage

You should now be able to upload daily data for your system, using the `-d` parameter to specify the date you would like to upload:


```bash
➜ ./solar -d 2018-12-02
processing 2018-12-02
posting chunk
success!
posting chunk
success!
```

The script will connect to Huawei, download the data for the date specified, convert the data to the format required by PVOutput, then upload the data in a number of chunks.

Note: currently `solar` will only work with paid PVOutput accounts.


