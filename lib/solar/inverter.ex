defmodule Solar.Inverter do
  @moduledoc """
  Downloads day data from fusionsolar.huawei.com
  """

  def download(date) do
    login()
    |> fetch_plant_data(date)
    |> handle_data_response
  end

  def info do
    login()
    |> fetch_plant_list
    |> handle_detail_response
  end

  defp login do
    "login"
    |> url
    |> HTTPoison.post(login_body(), header(), ssl: [verify: :verify_none])
    |> handle_login_response
  end

  defp fetch_plant_list({key, cookie}) do
    "queryPlantList"
    |> url
    |> HTTPoison.post(fetch_plant_list_body(key), header(), hackney: [cookie: [cookie]])
  end

  defp fetch_plant_data({key, cookie}, date) do
    "queryPlantDayData"
    |> url
    |> HTTPoison.post(fetch_plant_data_body(key, date), header(), hackney: [cookie: [cookie]])
  end

  defp fetch_device_detail({key, cookie}) do
    "queryDeviceDetail"
    |> url
    |> IO.inspect()
    |> HTTPoison.post(fetch_plant_detail_body(key), header(), hackney: [cookie: [cookie]])
  end

  defp handle_login_response({:ok, %HTTPoison.Response{body: body} = response}) do
    cookies =
      Enum.filter(response.headers, fn
        {"Set-Cookie", _} -> true
        _ -> false
      end)

    cookie = get_cookie(cookies)

    token =
      body
      |> Poison.decode()
      |> handle_ok

    {token, cookie}
  end

  defp handle_data_response({:ok, %HTTPoison.Response{body: body}}) do
    body
    |> Poison.decode!()
    |> Map.get("resultData")
  end

  defp handle_data_response({:error, %HTTPoison.Error{reason: reason}}) do
    IO.inspect("ERROR:")
    IO.inspect(reason)
  end

  defp handle_detail_response({:ok, %HTTPoison.Response{body: body}}) do
    body
    |> Poison.decode!()
    |> Map.get("resultData")
  end

  defp get_cookie([{_, value} | _]) do
    [jsession | _] = String.split(value, ";")
    jsession
  end

  defp handle_response({:error, error}) do
    {:error, error}
  end

  defp handle_ok({:ok, %{"openApiroarand" => key}}), do: key

  defp login_body do
    "userName=#{System.get_env("INVERTER_USERNAME")}&password=#{
      System.get_env("INVERTER_PASSWORD")
    }"
  end

  defp fetch_plant_list_body(key) do
    "openApiroarand=#{key}"
  end

  defp fetch_plant_data_body(key, date) do
    "plantid=#{System.get_env("INVERTER_PLANTID")}&date=#{date}&openApiroarand=#{key}"
  end

  defp fetch_plant_detail_body(key) do
    "plantid=#{System.get_env("INVERTER_PLANTID")}&openApiroarand=#{key}"
  end

  defp header do
    ["Content-Type": "application/x-www-form-urlencoded"]
  end

  def url(endpoint) do
    "https://au1.fusionsolar.huawei.com:27200/openApi/#{endpoint}"
  end
end
