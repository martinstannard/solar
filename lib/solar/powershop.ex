defmodule Solar.Powershop do
  @doc """
  interface function
  """
  def call(filename) do
    filename
    |> read
    |> parse
  end

  def read(filename) do
    File.stream!(filename) |> CSV.decode(headers: true)
  end

  def parse(data) do
    data
    |> Enum.chunk_every(2)
    |> Enum.map(fn rows ->
      IO.puts("")
      [{:ok, consumption}, {:ok, generation}] = rows
      IO.write(convert_date(consumption["DATE"]))
      IO.write(" ")
      IO.write("EXPORT ")
      gens = parse_day(generation)
      IO.write("IMPORT ")
      cons = parse_day(consumption)

      # IO.inspect(consumption)
      # IO.inspect(generation)

      # consumption
      # |> records(gens, cons)
      # |> List.flatten()
    end)
  end

  def parse_day(data) do
    # IO.write(convert_date(data["DATE"]))
    extract_net(data)
  end

  def extract_net(data) do
    {nets, total} =
      make_times()
      |> Enum.flat_map_reduce(0, fn t, acc ->
        current = trunc(String.to_float(Map.get(data, t)) * 1000)
        acc = acc + current
        {[{t, current}], acc}
      end)

    IO.write(total)
    IO.write(" ")
    nets
  end

  def records(data, cons, gens) do
    date = convert_date(data["DATE"])

    cons
    |> Enum.zip(gens)
    |> Enum.map(fn vals ->
      {{time, con}, {_, gen}} = vals
      # IO.inspect("con #{Enum.sum(con)}")
      hm = String.slice(time, 0..4)
      dup(date, hm, gen, con)
    end)
  end

  defp dup(date, <<hour::binary-size(2), ":", min::binary-size(2)>>, gen, con) when min == "00" do
    # build(date, hour, 0..1, gen, con)
    build(date, hour, 0..25, gen, con)
  end

  defp dup(date, <<hour::binary-size(2), ":", min::binary-size(2)>>, gen, con) when min == "30" do
    # build(date, hour, 30..31, gen, con)
    build(date, hour, 30..55, gen, con)
  end

  defp build(date, hour, range, gen, con) do
    range
    |> intervals
    |> Enum.map(fn m ->
      min = String.pad_leading("#{m}", 2, "0")
      # "#{date},#{hour}:#{min},-1,#{gen},-1,#{con}"
      # "#{date},#{hour}:#{min},-1,#{con},-1,#{gen}"
      # "#{date},#{hour}:#{min},-1,0,-1,#{gen - con}"
      # "#{date},#{hour}:#{min},-1,#{trunc(con / 6)},-1,#{trunc(gen / 6)}"
      "#{date},#{hour}:#{min},0,#{trunc(gen / 6)},-1,#{trunc(con / 6)}"
    end)
  end

  def intervals(range) do
    range
    |> Enum.chunk_every(1, 5)
    |> List.flatten()
  end

  def make_times do
    0..23
    |> Enum.map(fn h ->
      hr = String.pad_leading("#{h}", 2, "0")
      nh = rem(h + 1, 24)
      next_hr = String.pad_leading("#{nh}", 2, "0")
      ["#{hr}:00 - #{hr}:30", "#{hr}:30 - #{next_hr}:00"]
    end)
    |> List.flatten()
  end

  def convert_date(date) do
    [day, month, year] = String.split(date, "/")

    {:ok, d} = Date.new(String.to_integer(year), String.to_integer(month), String.to_integer(day))

    d
    |> Date.to_string()
    |> String.replace("-", "")
  end
end
