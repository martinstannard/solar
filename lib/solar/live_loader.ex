defmodule Solar.LiveLoader do
  def call(data) do
    data
    |> parse
    |> Enum.join("\n")
    |> IO.puts()
  end

  def parse(data) do
    data
    |> Enum.map(fn record ->
      record |> convert
    end)
  end

  def convert(record) do
    date_time =
      record
      |> Map.get("createTime")
      |> to_date_time

    energy =
      record
      |> Map.get("dayEnergy")

    "#{date_time}#{energy}"
  end

  def to_date_time(<<date::binary-size(10), " ", hm::binary-size(5), _::binary>>) do
    "#{hm},"
  end
end
