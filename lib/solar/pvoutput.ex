defmodule Solar.PvOutput do
  @moduledoc """
  takes a days worth of inverter readings, converts to a format accepted by pvoutput.org,
  chunks into groups of 100 records, then uploads to  pvoutput
  """

  @doc """
  interface function
  """
  def call(data) do
    data
    |> parse
    |> to_chunks
    |> post
  end

  def headers do
    [
      "X-Pvoutput-Apikey": System.get_env("PVOUTPUT_APIKEY"),
      "X-Pvoutput-SystemId": System.get_env("PVOUTPUT_ID"),
      "Content-Type": "application/x-www-form-urlencoded"
    ]
  end

  defp parse(data) do
    data
    |> Enum.map(fn record ->
      record |> convert
    end)
  end

  defp post(chunks) do
    chunks
    |> Enum.each(fn chunk ->
      IO.puts("posting chunk")

      url()
      |> HTTPoison.post(body(chunk), headers())
      |> handle_response
    end)
  end

  defp handle_response({:ok, %HTTPoison.Response{status_code: 200}}) do
    IO.puts("success!")
  end

  defp handle_response({:ok, response}) do
    IO.puts("post failed!")
    IO.inspect(response)
  end

  defp handle_response({:error, %HTTPoison.Error{reason: reason}}) do
    IO.puts("ERROR!")
    IO.inspect(reason)
  end

  defp to_chunks(data) do
    data
    |> Enum.chunk_every(100)
    |> Enum.map(fn records ->
      records
      |> Enum.join("")
    end)
  end

  defp convert(record) do
    date_time =
      record
      |> Map.get("createTime")
      |> to_date_time

    energy =
      record
      |> Map.get("dayEnergy")
      |> String.to_float()
      |> Kernel.*(1_000.0)
      |> Kernel.round()

    power =
      record
      |> Map.get("currentPower")
      |> String.to_float()
      |> Kernel.*(1_000.0)
      |> Kernel.round()

    "#{date_time}#{energy},#{power};"
  end

  defp to_date_time(<<date::binary-size(10), " ", hm::binary-size(5), _::binary>>) do
    d = String.replace(date, "-", "")
    "#{d},#{hm},"
  end

  defp url do
    "https://pvoutput.org/service/r2/addbatchstatus.jsp"
  end

  defp body(data) do
    "data=#{data}"
  end
end

# def power(days) do
#   days
#   |> Enum.each(fn day ->
#     day
#     |> Enum.chunk_every(100)
#     |> Enum.each(fn chunk ->
#       IO.inspect(chunk)

#       data =
#         Enum.join(chunk, ";")

#         # url()
#         # "https://pvoutput.org/service/r2/addbatchstatus.jsp?key=0188ec34fc58254d98f355d31008a8623bf1d31b&sid=59922&data=#{
#         #   data
#         # }&n=1"
#         # |> IO.inspect()
#         # |> HTTPoison.get(headers())
#         |> IO.inspect()

#       curly(data)
#     end)
#   end)
# end

# def curly(data) do
#   args = [
#     "-d",
#     "data=#{data}",
#     "-d",
#     "n=1",
#     "-H",
#     "X-Pvoutput-Apikey: 0188ec34fc58254d98f355d31008a8623bf1d31b",
#     "-H",
#     "X-Pvoutput-SystemId: 59922",
#     "https://pvoutput.org/service/r2/addbatchstatus.jsp"
#   ]

#   # ~s|-d 'data=#{data}' -d 'n=1' -H 'X-Pvoutput-Apikey: 0188ec34fc58254d98f355d31008a8623bf1d31b' -H 'X-Pvoutput-SystemId: 59922' https://pvoutput.org/service/r2/addbatchstatus.jsp|

#   IO.inspect(args)

#   System.cmd("curl", args)
#   |> IO.inspect()
# end
