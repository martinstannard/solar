defmodule Solar do
  @moduledoc """
  Documentation for Solar.
  """

  @doc """
  takes an optional date param, fetches data from inverter site, converts and uploads to
  pvoutput.org
  """

  def main(args) do
    opts = parse_arguments(args)

    run(opts)
  end

  def run(info: true) do
    IO.puts("running info")
    IO.inspect(Solar.Inverter.info())
  end

  def run(date: date) do
    IO.puts("processing #{date}")

    date
    |> convert_date
    |> process_day
  end

  def run(_) do
    IO.puts("processing today")

    Date.utc_today()
    |> process_day
  end

  def process_day(date) do
    date
    |> Solar.Inverter.download()
    |> handle_response()
  end

  def handle_response("") do
    IO.inspect("NO DATA RETURNED")
    {:error, "no data"}
  end

  def handle_response(data) do
    data
    |> Solar.PvOutput.call()
  end

  defp convert_date(date) do
    {:ok, new_date} = Date.from_iso8601(date)
    new_date
  end

  def power_up() do
    "nov.csv"
    |> Solar.Powershop.call()

    nil

    # |> Solar.PvOutput.power()
  end

  defp parse_arguments(args) do
    {opts, _, _} =
      OptionParser.parse(args,
        switches: [date: :string, info: :boolean],
        aliases: [d: :date, i: :info]
      )

    opts
  end
end
